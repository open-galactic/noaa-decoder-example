# NOAA Decoder Example

An example of how to use a satellite decoder with methods included for public NOAA spacecraft. 


Contains:

- `examples/noaa/`: NOAA decoder, examples and test data
- `osso_satellites/`: decoder utils, functions and tools
- `example_wav_to_image.py`: Python script example wav conversion


Requires:

- Python 3 (Recommended 3.9)

## Development

### Initial config


Setup & configure python:

```
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```



## Obtaining Data

For the example in this repository, a user needs to obtain data one or two ways:

## Using a HAM Radio

The data will be an audio file. An example of this is shown in the examples directory, to use this on your own data:

```
python example_wav_to_image.py
```


## Using an SDR

The data will be an IQ radio file in the form of a two-channel wav file:
```
from scipy.io.wavfile import read

from osso_satellites.decoders.noaa.apt.wbfm import wbfm_decode_chunks

samp_rate, signal = read('path/to/your/recorded/IQ.wav')

audio = wbfm_decode_chunks(*signal.T, samp_rate)

image = audio_decoder(samp_rate, audio)

image.save('examples/noaa/noaa_example_image.png')
```

If doppler correction needs to be performed on the signal and the user has access to the TLE at the time of recording, a utility function is available to correct the data:
```
from osso_satellites.decoders.utils.doppler_shifting import shift_signal

shift_signal(signal, samp_rate, 
             tle, longitude, latitude, altitude, 
             centre_frequency, starttime,
             corrections_per_sec = 1)
```

There is also a utility function for plotting the waterfall of IQ data. This is a good method for assessing signal strength.
