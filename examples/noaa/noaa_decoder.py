import json
from datetime import datetime, timedelta
from dateutil.parser import parse
import numpy as np
from scipy.io.wavfile import read
from PIL import Image

from osso_satellites.decoders.noaa.apt.audio_decoder import audio_decoder
from osso_satellites.decoders.noaa.apt.geometry import find_img_latlons
from osso_satellites.decoders.noaa.apt.mapping import draw_boundaries


def test_audio_decode():
    samp_rate, signal = read('examples/noaa/noaa_example_audio.wav')

    image = audio_decoder(samp_rate, signal)

    image.save('examples/noaa/noaa_example_image.png')

        

def test_image_processing():
    noaa_raw_image = np.array(Image.open('examples/noaa/noaa_example_image.png'))

    left_image = noaa_raw_image[:,:1040]
    right_image = noaa_raw_image[:,1040:]

    left_image = left_image[:,39+47:-45]
    right_image = right_image[:,39+47:-45]

    starttime = parse('2021-06-19T11:01:09')
    endtime = parse('2021-06-19T11:15:33')

    line0 = '0 NOAA 15'
    line1 = '1 25338U 98030A   21169.55924016  .00000069  00000-0  47205-4 0  9999'
    line2 = '2 25338  98.6812 198.0744 0009176 278.4478  81.5662 14.26038268201150'

    lons, lats = find_img_latlons(starttime, endtime,
                                 (line0, line1, line2), 
                                 noaa_raw_image.shape[0])

    for image, image_key in zip([left_image, right_image], ['examples/noaa/visible_light_with_borders.png', 'examples/noaa/ir_with_borders.png']):
        image = draw_boundaries(lons, lats, image)
        image = Image.fromarray(image)
        image.save(image_key, format='PNG')


if __name__=='__main__':
    test_audio_decode()
    test_image_processing()