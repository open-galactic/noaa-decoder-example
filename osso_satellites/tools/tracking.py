from datetime import datetime, timedelta
from pyorbital.orbital import Orbital
from pyorbital.astronomy import observer_position
import numpy as np
from math import *

SPEED_OF_LIGHT = 299792458.0
KM_TO_M = 1000.0


def overpass_time_info(query_time,
                       line0,
                       line1,
                       line2,
                       longitude,
                       latitude,
                       altitude,
                       threshold,
                       period = 24):
    '''
    returns a list of rise and fall times for
    a leo satellite over a given period (default 24 hours)
    '''

    try:
        # define an orbital object given the tle
        orb = Orbital(line0, line1=line1, line2=line2)

        tle_overpass_list = orb.get_next_passes(
            utc_time = query_time,
            length = period,
            lon = longitude,
            lat = latitude,
            alt = altitude,
            horizon = threshold)

        return tle_overpass_list

    except NotImplementedError:
        return []


def overpass_leapfrog_path(query_time,
                           line0,
                           line1,
                           line2,
                           longitude,
                           latitude,
                           altitude,
                           threshold,
                           period = 24):
    '''
    breaks up a uniform second-by-second overpass
    into a uniform error list (5 degree cosine error beteen
    each az-el setpoint).
    '''

    overpass_list_dict = []

    # define an orbital object given the tle
    try:
        orb = Orbital(line0, line1=line1, line2=line2)

        overpass_list = overpass_time_info(query_time,
                                        line0,
                                        line1,
                                        line2,
                                        longitude,
                                        latitude,
                                        altitude,
                                        threshold,
                                        period)
        
        for rise_time, fall_time, max_el_time in overpass_list:
            overpass_dict = dict(start_time = rise_time.isoformat(),
                                maximum_elevation_time = max_el_time.isoformat(),
                                end_time = fall_time.isoformat(),
                                azel = [],
                                doppler = [],
                                timestamps = [])

            azimuth, elevation = orb.get_observer_look(max_el_time, longitude, latitude, altitude)

            overpass_dict['maximum_elevation'] = elevation

            num_secs = 0.0
            while elevation > threshold:
                timestamp = max_el_time - timedelta(seconds=num_secs)

                # calculate the azimuth and elevation for the
                # given timestamp
                azimuth, elevation = orb.get_observer_look(timestamp,
                                                        longitude, 
                                                        latitude,
                                                        altitude)

                # extract information for range rate calculation
                sat_pos, sat_vel = orb.get_position(timestamp, normalize=False)
                obs_pos, obs_vel = observer_position(timestamp, 
                                                    longitude, 
                                                    latitude,
                                                    altitude)

                # calculate the relative poristion of the satellite 
                # from the groundstation
                rel_pos = KM_TO_M * (sat_pos - obs_pos)
                rel_vel = KM_TO_M * (sat_vel - obs_vel)

                # if the error between the last is greater than 5 degrees 
                # then add it to the overpass list
                if (len(overpass_dict['azel']) == 0)or(cosine_error(*overpass_dict['azel'][0], azimuth, elevation) > 5.0)or(elevation < threshold):
                    overpass_dict['doppler'].insert(0,
                        np.dot(rel_pos, rel_vel) / (np.sqrt(np.dot(rel_pos, rel_pos)) * SPEED_OF_LIGHT))
                    overpass_dict['azel'].insert(0,[azimuth, elevation])
                    overpass_dict['timestamps'].insert(0,timestamp.isoformat())

                num_secs += 1

            num_secs = 0.0
            azimuth, elevation = overpass_dict['azel'][-1]
            while elevation > threshold:
                timestamp = max_el_time + timedelta(seconds=num_secs)

                # calculate the azimuth and elevation for the
                # given timestamp
                azimuth, elevation = orb.get_observer_look(timestamp,
                                                        longitude, 
                                                        latitude,
                                                        altitude)

                # extract information for range rate calculation
                sat_pos, sat_vel = orb.get_position(timestamp, normalize=False)
                obs_pos, obs_vel = observer_position(timestamp, 
                                                    longitude, 
                                                    latitude,
                                                    altitude)

                # calculate the relative position of the satellite 
                # from the groundstation
                rel_pos = KM_TO_M * (sat_pos - obs_pos)
                rel_vel = KM_TO_M * (sat_vel - obs_vel)

                # if the error between the last is greater than 5 degrees 
                # then add it to the overpass list
                if (len(overpass_dict['azel']) == 0)or(cosine_error(*overpass_dict['azel'][-1], azimuth, elevation) > 5.0)or(elevation < threshold):
                    overpass_dict['doppler'].append(
                        np.dot(rel_pos, rel_vel) / (np.sqrt(np.dot(rel_pos, rel_pos)) * SPEED_OF_LIGHT))
                    overpass_dict['azel'].append([azimuth, elevation])
                    overpass_dict['timestamps'].append(timestamp.isoformat())

                num_secs += 1

            overpass_list_dict.append(overpass_dict)

        return overpass_list_dict
    
    except NotImplementedError:
        return []

    
def cosine_error(az0,
                 el0,
                 az1,
                 el1):
    '''
    gives the cosine error between two az-el setpoints
    '''

    x0 = [cos(np.deg2rad(az0)) * cos(np.deg2rad(el0)),
            sin(np.deg2rad(az0)) * cos(np.deg2rad(el0)),
            sin(np.deg2rad(el0))]

    x1 = [cos(np.deg2rad(az1)) * cos(np.deg2rad(el1)),
            sin(np.deg2rad(az1)) * cos(np.deg2rad(el1)),
            sin(np.deg2rad(el1))]

    return np.rad2deg(np.arccos(np.dot(x0, x1)))


