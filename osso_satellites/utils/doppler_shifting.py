import numpy as np
import pyorbital
from pyorbital.orbital import Orbital
from pyorbital.astronomy import observer_position
from datetime import datetime, timedelta

SPEED_OF_LIGHT = 299792458.0

def shift_signal(signal, 
                 samp_rate, 
                 tle, 
                 longitude,
                 latitude, 
                 altitude, 
                 centre_frequency,
                 starttime,
                 corrections_per_sec = 1):

    signal -= np.mean(signal)

    line0, line1, line2 = tle

    orb = Orbital(line0, line1=line1, line2=line2)

    frequency = np.zeros(len(signal))
    for i in range(len(signal)):
        if i % (samp_rate // corrections_per_sec) == 0:
            print(i / samp_rate)
            t = starttime + timedelta(seconds=i/samp_rate)

            sat_pos, sat_vel = orb.get_position(t, normalize=False)
            obs_pos, obs_vel = observer_position(t, longitude, latitude, altitude)

            rel_pos = 1000.0*(sat_pos - obs_pos)
            rel_vel = 1000.0*(sat_vel - obs_vel)

            d = np.dot(rel_pos, rel_vel) / (np.sqrt(np.dot(rel_pos, rel_pos)) * SPEED_OF_LIGHT)

        frequency[i] = d * centre_frequency

    theta = 4.0 * np.pi * frequency / samp_rate
    theta = np.mod(np.cumsum(theta), 2.0 * np.pi)

    signal *= np.exp(-1.0j * theta)

    return signal
