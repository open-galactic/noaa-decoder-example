import numpy as np
from PIL import Image
import scipy.io.wavfile
from scipy.signal import decimate

def waterfall(signal, samp_rate, decimation=1, N=2048):
    samp_rate = samp_rate // decimation
    waterfall_data = []

    for k in range(N, 
                len(signal) - N,
                N // 2):
        signal_ = signal[k:k+N,0]+1.0j*signal[k:k+N,1]
        signal_ -= np.mean(signal_)
        if decimation != 1:
            signal_ = decimate(signal_, decimation)
        signal_spectrum = np.abs(np.fft.fftshift(np.fft.fft(signal_, N)))
        freqs = np.fft.fftshift(np.fft.fftfreq(N, d=decimation/samp_rate))

        waterfall_data.append(signal_spectrum)

    waterfall_data = np.array([np.mean(waterfall_data[i:i+10], axis=0) for i in range(0,len(waterfall_data),10)])

    waterfall_data = 255 * waterfall_data / (N/2)
    waterfall_data = waterfall_data.astype(np.uint8)

    signal_length = len(signal) / samp_rate

    return waterfall_data, (np.min(freqs), np.max(freqs)), signal_length