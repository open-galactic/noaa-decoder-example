import numpy as np
from datetime import datetime, timedelta
from pyorbital import astronomy
from pyorbital.orbital import Orbital

FOCAL_LENGTH = 305.0
EARTH_RADIUS = 6378135.0

def km_to_m(x,y,z):
    return x*1000.0, y*1000.0, z*1000.0

def m_to_km(x,y,z):
    return x/1000.0, y/1000.0, z/1000.0

def get_lonlatalt(pos_x, pos_y, pos_z, utc_time):
    lon = ((np.arctan2(pos_y, pos_x) - astronomy.gmst(utc_time))
            % (2 * np.pi))

    lon = np.where(lon > np.pi, lon - np.pi * 2, lon)
    lon = np.where(lon <= -np.pi, lon + np.pi * 2, lon)

    r = np.sqrt(pos_x ** 2 + pos_y ** 2)
    lat = np.arctan2(pos_z, r)

    return np.rad2deg(lon), np.rad2deg(lat)

def rot(a,b):
    v0, v1, v2 = np.cross(a, b)
    c = np.dot(a, b)

    skew_vel = np.array([[0.0,-v2, v1],
                         [ v2,0.0,-v0],
                         [-v1, v0,0.0]])
    skew_vel_sq = np.array([[-v2*v2-v1*v1,        v0*v1,        v2*v0],
                            [       v0*v1, -v2*v2-v0*v0,        v2*v1],
                            [       v0*v2,        v1*v2, -v1*v1-v0*v0]])
    R = np.eye(3) + skew_vel + skew_vel_sq / (1 + c)

    return R

def rot_from_posvel(pos, vel):
    xbody, ybody, zbody = np.array([[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]])
    normed_vel = vel / np.linalg.norm(vel)
    normed_pos = pos / np.linalg.norm(pos)

    Rpos_z = rot(zbody, normed_pos)

    vel_z = np.linalg.inv(Rpos_z).dot(normed_vel)

    Rvel_z = rot(xbody, vel_z)

    return Rpos_z.dot(Rvel_z)

def find_latlon_row(utc_time, pos, vel):
    pos *= 1000.0
    vel *= 1000.0
    R = rot_from_posvel(pos, vel)

    lonlats = []

    for mu in np.arange(-454, 455, 1):
        meas = np.array([0.0, mu, -FOCAL_LENGTH])
        fp = R.dot(meas)

        a = np.sum(np.square(fp))
        b = 2.0 * (np.dot(fp, pos))
        c = np.sum(np.square(pos)) - (EARTH_RADIUS * EARTH_RADIUS)

        det = b*b - 4.0*a*c

        if det > 0:
            det = np.sqrt(det)

            tl, tu = 1/(2*a) * (-b + np.array([-det, det]))
            t = np.min([tl, tu])
        
            x,y,z = (pos + fp*t)

            lonlats.append(get_lonlatalt(x, y, z, utc_time))
        else:
            lonlats.append([np.nan, np.nan])

    return np.array(lonlats)

def find_img_latlons(starttime, endtime, tle, N):
    lon, lat = ([],[])
    line0, line1, line2 = tle

    num_secs = (endtime - starttime).total_seconds()
    midtime = starttime + timedelta(seconds=num_secs/2)

    orb = Orbital(line0, line1=line1, line2=line2)

    for py in range(-N//2, N//2, 1):
        t = midtime + timedelta(seconds=py/2)
        p, v = orb.get_position(t, normalize=False)
        lon_, lat_ = find_latlon_row(t, p, v).T
        lon.append(lon_)
        lat.append(lat_)

    return np.array(lon), np.array(lat)
