import os
import numpy as np
import shapefile
from PIL import Image, ImageDraw
from scipy.interpolate import griddata

def draw_boundaries(lons, lats, img):
    clon = lons[lons.shape[0]//2, lons.shape[1]//2]
    clat = lats[lats.shape[0]//2, lats.shape[1]//2]

    x = np.cos(np.deg2rad(lons)) * np.cos(np.deg2rad(lats))
    y = np.sin(np.deg2rad(lons)) * np.cos(np.deg2rad(lats))
    z = np.sin(np.deg2rad(lats))

    d = np.concatenate([x.reshape(*x.shape,1),
                        y.reshape(*y.shape,1),
                        z.reshape(*z.shape,1)], axis=2)

    r = np.deg2rad(clon)
    Rlon = np.array([[np.cos(r), -np.sin(r), 0.0],
                     [np.sin(r),  np.cos(r), 0.0],
                     [      0.0,        0.0, 1.0]])
    d = np.dot(d, Rlon)

    r = np.deg2rad(clat)
    Rlat = np.array([[np.cos(r),  0.0, -np.sin(r)],
                     [      0.0,  1.0,        0.0],
                     [np.sin(r),  0.0,  np.cos(r)]])
    d = np.dot(d, Rlat)

    # find more data at https://github.com/nvkelso/natural-earth-vector
    sf = shapefile.Reader(os.path.join(os.path.dirname(__file__), 'mapping_data/ne_50m_admin_0_countries/ne_50m_admin_0_countries'))
    shapes = sf.shapes()

    yimg = np.linspace(-0.35, 0.35, 1400)
    zimg = np.linspace(0.4, -0.4, 1600)

    Yimg, Zimg = np.meshgrid(yimg, zimg)

    img = griddata((d[:,:,1].flatten(), d[:,:,2].flatten()), 
                   img.flatten(), 
                   (Yimg, Zimg)).reshape(1600,1400)

    img = np.repeat(img[:, :, np.newaxis], 3, axis=2).astype(np.uint8)
    img = Image.fromarray(img)

    draw = ImageDraw.Draw(img)

    for shape in shapes:
        cx, cy = np.deg2rad(np.array(shape.points)).T

        c = np.array([np.cos(cx) * np.cos(cy),
                      np.sin(cx) * np.cos(cy),
                      np.sin(cy)]).T
        c = np.dot(np.dot(c, Rlon), Rlat)
        c[c[:,0] < 0] = np.nan

        try:
            c[:,1] = 1400.0 * (c[:,1] + 0.35) / 0.7
            c[:,2] = 1600 - 1600.0 * (c[:,2] + 0.4) / 0.8

            for cy1,cz1,cy2,cz2 in zip(*c[1:,1:].T, *c[:-1,1:].T):
                if (0 < cy1 < 1400)and(0 < cy2 < 1400):
                    if (0 < cz1 < 1600)and(0 < cz2 < 1600):
                        if np.sqrt((cy2 - cy1)*(cy2 - cy1) + (cz2 - cz1)*(cz2 - cz1)) < 50:
                            draw.line((cy1, cz1, cy2, cz2), fill=(255, 255, 255), width=2)

        except Exception as e:
            print(e)

    return np.array(img)