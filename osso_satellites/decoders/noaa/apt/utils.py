import numpy as np
from .telemetry import process_telem

def rescale_data(img_array,
                 telemetry_frame):
    raw_data = process_telem(telemetry_frame)

    scaled_image = np.clip(255.0 * (img_array - raw_data[8]) / (raw_data[7] - raw_data[8]), 0.0, 255.0)
    scaled_data = 255.0 * (raw_data - raw_data[8]) / (raw_data[7] - raw_data[8])

    return scaled_image, scaled_data