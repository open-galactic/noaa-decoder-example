import numpy as np

TELEMETRY_SYNC = np.repeat([31, 63, 95, 127, 159, 191, 223, 255, 0], 8)

def find_frame(_telemetry_frame):    
    peaks = [(0, 0)]
    mindistance = 20000

    for i in range(len(_telemetry_frame)-len(TELEMETRY_SYNC)):
        corr = np.dot(TELEMETRY_SYNC, _telemetry_frame[i : i+len(TELEMETRY_SYNC)])

        if i - peaks[-1][0] > mindistance:
            peaks.append((i, corr))

        elif corr > peaks[-1][1]:
            peaks[-1] = (i, corr)

    return peaks[0]

def process_telem(telemetry_frame):
    _telemetry_frame = np.mean(telemetry_frame, axis=1)

    sync_id, _ = find_frame(_telemetry_frame)

    data = np.mean(_telemetry_frame[sync_id:sync_id+16*8].reshape(-1,8), axis=1)

    return data