import numpy
import scipy.io.wavfile
import scipy.signal
from PIL import Image
from .filters import butter_bandpass_filter
from .noaa_constants import *

RATE = NOAA_AUDIO_SAMP_RATE
NOAA_LINE_LENGTH = NOAA_LINE_LENGTH

def resample(rate, signal):
    if rate != RATE:
        coef = RATE / rate
        samples = int(coef * len(signal))
        signal = scipy.signal.resample(signal, samples)
    
    return signal

def process_audio(rate, signal):
    if rate != RATE:
        signal = resample(rate, signal)
    remainder = len(signal) % rate
    
    if remainder != 0:
        signal = signal[:-remainder]

    return RATE, signal

def digitize(signal, plow=0.5, phigh=99.5):
    (low, high) = numpy.percentile(signal, (plow, phigh))
    delta = high - low
    data = numpy.round(255 * (signal - low) / delta)
    data[data < 0] = 0
    data[data > 255] = 255
    return data

def decode_signal(signal, num_cols=2080):
    signal = butter_bandpass_filter(signal, 500, 4600, RATE)
    signal = scipy.signal.hilbert(signal)
    signal = scipy.signal.medfilt(numpy.abs(signal), 5)
    signal = numpy.mean(signal.reshape(-1,5), axis=1)

    num_rows = len(signal) // 2080
    
    signal = signal[:num_cols*num_rows]
    signal = digitize(signal)

    image_array = signal.reshape(-1, 2080)
    image_array = sync_image(image_array)

    return image_array

def sync_image(image):
    sync = [0, 128, 255, 128]*7 + [0]*7
    sync = [x-128 for x in sync]

    for i,row in enumerate(image):
        sync_corr = scipy.signal.correlate(row - 128, sync, mode='same')
        image[i] = numpy.roll(row, -numpy.argmax(sync_corr) + len(sync) // 2)

    return image

def audio_decoder(rate, signal):
    rate, signal = process_audio(rate, signal)
    image = decode_signal(signal).astype(numpy.uint8)
    image = Image.fromarray(image)
    return image
