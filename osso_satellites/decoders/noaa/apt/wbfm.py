import numpy as np
import scipy.signal
from .filters import butter_bandpass_filter
from .noaa_constants import *

def wbfm_decode_chunks(I,
                       Q,
                       samp_rate,
                       out_rate = NOAA_AUDIO_SAMP_RATE,
                       bw = NOAA_APT_BANDWIDTH,
                       n_taps = 64,
                       chunk_len_sec = 10.0):
    chunk_len_sec = 10.0
    step = int(chunk_len_sec * samp_rate)

    signal = []

    for i in range(0, len(I), step):
        signal += list(wbfm_decode(I[i:i+step],
                                   Q[i:i+step],
                                   samp_rate,
                                   out_rate,
                                   bw,
                                   n_taps))
    
    return np.array(signal)

def wbfm_decode(I,
                Q,
                samp_rate,
                out_rate = NOAA_AUDIO_SAMP_RATE,
                bw = NOAA_APT_BANDWIDTH,
                n_taps = 64):
    signal = I + 1.0j*Q

    lpf = scipy.signal.remez(n_taps, [0, bw, bw+(samp_rate/2-bw)/4, samp_rate/2], [1,0], Hz=samp_rate)  
    signal = scipy.signal.lfilter(lpf, 1.0, signal)
    signal -= np.mean(signal)
    signal = np.angle(signal[1:] * np.conj(signal[:-1]))

    d = samp_rate * 75e-6  
    z = np.exp(-1/d)
    b = [1-z]
    a = [1,-z]  
    signal = scipy.signal.lfilter(b,a,signal)

    signal = butter_bandpass_filter(signal, 500.0, 3600.0, samp_rate)

    u = np.quantile(signal, 0.975)
    signal = np.clip(signal, -u, u)
    signal /= u

    coef = out_rate / samp_rate

    signal_ = []
    for j in range(0,len(signal),samp_rate):
        chunk = signal[j:j+samp_rate]
        samples = int(coef * len(chunk))
        signal_ += list(scipy.signal.resample(chunk, samples))

    signal = np.array(signal_)

    return signal

def write_to_16bit(signal):
    signal = np.clip(signal, -1.0, 1.0)
    signal *= np.iinfo(np.int16).max
    return signal.astype(np.int16)
