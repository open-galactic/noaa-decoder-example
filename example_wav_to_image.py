from scipy.io.wavfile import read

from osso_satellites.decoders.noaa.apt.audio_decoder import audio_decoder

samp_rate, signal = read('examples/noaa/noaa_example_audio.wav')

image = audio_decoder(samp_rate, signal) # display image with image.show()

image.save('examples/noaa/output.png')
